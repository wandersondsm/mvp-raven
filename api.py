from flask import Flask, jsonify

app = Flask(__name__)

# Dados de exemplo: lista de produtos
produtos = [
    {"id": 1, "nome": "Laptop", "preco": 1500.00},
    {"id": 2, "nome": "Smartphone", "preco": 800.00},
    {"id": 3, "nome": "Tablet", "preco": 300.00},
    {"id": 4, "nome": "Monitor", "preco": 200.00},
    {"id": 5, "nome": "Teclado", "preco": 50.00}
]

@app.route('/api/produtos', methods=['GET'])
def get_produtos():
    return jsonify(produtos)

if __name__ == '__main__':
    app.run(debug=True)
