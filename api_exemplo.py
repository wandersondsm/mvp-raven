from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import List, Optional

app = FastAPI()

class Contrato(BaseModel):
    id: int
    nome: str
    valor: int
    descricao: Optional[str] = None
    contratante_cnpj: str
    contratada_cnpj: str
    prazo: int

# Banco de dados em memória
banco_de_dados = []

@app.post("/contratos/", response_model=Contrato)
def criar_contrato(contrato: Contrato):
    banco_de_dados.append(contrato)
    return contrato

@app.get("/contratos/", response_model=List[Contrato])
def listar_contratos():
    return banco_de_dados

@app.get("/contratos/{contrato_id}", response_model=Contrato)
def obter_contrato(contrato_id: int):
    for contrato in banco_de_dados:
        if contrato.id == contrato_id:
            return contrato
    raise HTTPException(status_code=404, detail="Contrato não encontrado")

@app.put("/contratos/{contrato_id}", response_model=Contrato)
def atualizar_contrato(contrato_id: int, contrato_atualizado: Contrato):
    for index, contrato in enumerate(banco_de_dados):
        if contrato.id == contrato_id:
            banco_de_dados[index] = contrato_atualizado
            return contrato_atualizado
    raise HTTPException(status_code=404, detail="Contrato não encontrado")

@app.delete("/contratos/{contrato_id}", response_model=Contrato)
def deletar_contrato(contrato_id: int):
    for index, contrato in enumerate(banco_de_dados):
        if contrato.id == contrato_id:
            contrato_removido = banco_de_dados.pop(index)
            return contrato_removido
    raise HTTPException(status_code=404, detail="Contrato não encontrado")

def formatar_cnpj(cnpj):
    # Remove todos os caracteres não numéricos
    cnpj = ''.join(filter(str.isdigit, str(cnpj)))
    
    # Verifica se o CNPJ tem 14 dígitos
    if len(cnpj) != 14:
        raise ValueError("O CNPJ deve ter 14 dígitos.")

    # Formata o CNPJ no padrão 00.000.000/0000-00
    cnpj_formatado = f"{cnpj[:2]}.{cnpj[2:5]}.{cnpj[5:8]}/{cnpj[8:12]}-{cnpj[12:14]}"
    
    return cnpj_formatado

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
