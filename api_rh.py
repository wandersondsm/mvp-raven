from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/')
def home():
    return "Bem-vindo à API do Sistema Integrado de Gestão do Atendimento ao Cidadão!"

@app.route('/receber_dados', methods=['POST'])
def receber_dados():
    try:
        # Obtém os dados enviados na requisição
        dados = request.get_json()

        # Verifica se os dados foram enviados corretamente
        if not dados:
            return jsonify({'mensagem': 'Nenhum dado recebido'}), 400

        # Processa os dados conforme necessário (aqui apenas imprimimos)
        print(f'Dados recebidos: {dados}')

        # Retorna uma resposta de sucesso
        return jsonify({'mensagem': 'Dados recebidos com sucesso', 'dados': dados}), 200

    except Exception as e:
        return jsonify({'mensagem': 'Erro ao processar os dados', 'erro': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
