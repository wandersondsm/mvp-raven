from elasticsearch import Elasticsearch, helpers

class ElasticSearchClient:
    def __init__(self, hosts=None):
        """
        Inicializa a conexão com o Elasticsearch.

        :param hosts: Lista de hosts do Elasticsearch. Exemplo: ['localhost:9200']
        """
        if hosts is None:
            hosts = ['localhost:9200']
        self.client = Elasticsearch(hosts)

    def create_index(self, index_name, settings=None):
        """
        Cria um índice no Elasticsearch.

        :param index_name: Nome do índice a ser criado.
        :param settings: Configurações do índice. (opcional)
        :return: Resposta da criação do índice.
        """
        if settings is None:
            settings = {
                "settings": {
                    "number_of_shards": 1,
                    "number_of_replicas": 0
                }
            }
        return self.client.indices.create(index=index_name, body=settings)

    def delete_index(self, index_name):
        """
        Deleta um índice no Elasticsearch.

        :param index_name: Nome do índice a ser deletado.
        :return: Resposta da deleção do índice.
        """
        return self.client.indices.delete(index=index_name)