from elasticsearch import Elasticsearch, helpers

class ElasticSearchClient:
    def __init__(self, hosts=None):
        """
        Inicializa a conexão com o Elasticsearch.

        :param hosts: Lista de hosts do Elasticsearch. Exemplo: ['localhost:9200']
        """
        if hosts is None:
            hosts = ['localhost:9200']
        self.client = Elasticsearch(hosts)

    def create_index(self, index_name, settings=None):
        """
        Cria um índice no Elasticsearch.

        :param index_name: Nome do índice a ser criado.
        :param settings: Configurações do índice. (opcional)
        :return: Resposta da criação do índice.
        """
        if settings is None:
            settings = {
                "settings": {
                    "number_of_shards": 1,
                    "number_of_replicas": 0
                }
            }
        return self.client.indices.create(index=index_name, body=settings)

    def delete_index(self, index_name):
        """
        Deleta um índice no Elasticsearch.

        :param index_name: Nome do índice a ser deletado.
        :return: Resposta da deleção do índice.
        """
        return self.client.indices.delete(index=index_name)

    def index_document(self, index_name, doc_type, document, doc_id=None):
        """
        Indexa um documento no Elasticsearch.

        :param index_name: Nome do índice.
        :param doc_type: Tipo do documento.
        :param document: Documento a ser indexado (dicionário).
        :param doc_id: ID do documento (opcional).
        :return: Resposta da indexação.
        """
        return self.client.index(index=index_name, doc_type=doc_type, body=document, id=doc_id)

    def get_document(self, index_name, doc_type, doc_id):
        """
        Obtém um documento do Elasticsearch.

        :param index_name: Nome do índice.
        :param doc_type: Tipo do documento.
        :param doc_id: ID do documento.
        :return: Documento obtido.
        """
        return self.client.get(index=index_name, doc_type=doc_type, id=doc_id)

    def search(self, index_name, query):
        """
        Realiza uma busca no Elasticsearch.

        :param index_name: Nome do índice.
        :param query: Consulta a ser realizada (dicionário).
        :return: Resultados da busca.
        """
        return self.client.search(index=index_name, body=query)
