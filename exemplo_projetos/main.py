from fastapi import FastAPI, Depends, HTTPException, status
from sqlalchemy.orm import Session
from typing import List

from . import crud, models, schemas, database

models.Base.metadata.create_all(bind=database.engine)

app = FastAPI()

@app.post("/contracts/", response_model=schemas.Contract, status_code=status.HTTP_201_CREATED)
def create_contract(contract: schemas.ContractCreate, db: Session = Depends(database.get_db)):
    return crud.create_contract(db=db, contract=contract)

@app.get("/contracts/", response_model=List[schemas.Contract])
def read_contracts(skip: int = 0, limit: int = 10, db: Session = Depends(database.get_db)):
    contracts = crud.get_contracts(db, skip=skip, limit=limit)
    return contracts

@app.get("/contracts/{contract_id}", response_model=schemas.Contract)
def read_contract(contract_id: int, db: Session = Depends(database.get_db)):
    db_contract = crud.get_contract(db, contract_id=contract_id)
    if db_contract is None:
        raise HTTPException(status_code=404, detail="Contract not found")
    return db_contract

@app.put("/contracts/{contract_id}", response_model=schemas.Contract)
def update_contract(contract_id: int, contract: schemas.ContractUpdate, db: Session = Depends(database.get_db)):
    db_contract = crud.update_contract(db, contract_id=contract_id, contract=contract)
    if db_contract is None:
        raise HTTPException(status_code=404, detail="Contract not found")
    return db_contract

@app.delete("/contracts/{contract_id}", response_model=schemas.Contract)
def delete_contract(contract_id: int, db: Session = Depends(database.get_db)):
    db_contract = crud.delete_contract(db, contract_id=contract_id)
    if db_contract is None:
        raise HTTPException(status_code=404, detail="Contract not found")
    return db_contract
