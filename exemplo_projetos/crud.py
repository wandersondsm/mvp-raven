from pydantic import BaseModel
from datetime import date

class ContractBase(BaseModel):
    name: str
    start_date: date
    end_date: date

class ContractCreate(ContractBase):
    pass

class ContractUpdate(ContractBase):
    pass

class Contract(ContractBase):
    id: int

    class Config:
        orm_mode = True
