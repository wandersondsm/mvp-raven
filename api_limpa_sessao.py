from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/')
def home():
    return "Bem-vindo à API do Sistema Integrado de Gestão do Atendimento ao Cidadão!"

@app.route('/rotina/limpaSessaoExpirada', methods=['POST'])
def limpa_sessao_expirada():
    try:
        # Obtém o código de validação da query string
        auth_code = request.args.get('auth')
        
        # Verifica se o código de validação foi enviado
        if not auth_code:
            return jsonify({'mensagem': 'Código de validação não fornecido'}), 400

        # Lógica para limpeza de sessão expirada (placeholder)
        # Aqui você deve implementar a lógica real para limpar as sessões expiradas
        print(f'Código de validação recebido: {auth_code}')
        
        # Resposta de sucesso
        return jsonify({'mensagem': 'Sessões expiradas removidas com sucesso'}), 200

    except Exception as e:
        return jsonify({'mensagem': 'Erro ao processar a solicitação', 'erro': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
