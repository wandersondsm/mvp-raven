from elasticsearch import Elasticsearch, helpers

class ElasticSearchClient:
    def __init__(self, hosts=None):
        if hosts is None:
            hosts = ['localhost:9200']
        self.client = Elasticsearch(hosts)

    def create_index(self, index_name, settings=None):
        if settings is None:
            settings = {
                "settings": {
                    "number_of_shards": 1,
                    "number_of_replicas": 0
                }
            }
        return self.client.indices.create(index=index_name, body=settings)

    def delete_index(self, index_name):
        return self.client.indices.delete(index=index_name)
